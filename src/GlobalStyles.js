import styled, { createGlobalStyle } from 'styled-components';

export const theme = {
  primary: '#dadada',
  secondary: 'gray',
  background: '#DEEEF2',
  textColor: '#919497',
};

export const globalVars = {
  headerHeight: '70px',
  headerWidthPercentage: '95%',
};

export const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GlobalStyle = createGlobalStyle`
  html {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
  }

  body {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
    padding: 0;
    font-size: 16px;
    font-family: "Roboto", sans-serif;
    letter-spacing: 0.05rem;
    background-color: rgba(222, 224, 233, 0.01);
    line-height: 28px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    overflow: hidden;
  }

  h1 {
    font-size: 2.375em;
    font-weight: bold;
    color: ${(props) => props.theme.textColor};
    line-height: 1.5em;
    margin: 0;
    padding-bottom: 25px;
    box-sizing: border-box;
  }

  h2 {
    font-size: 2em;
    font-weight: 500;
    color: ${(props) => props.theme.textColor};
    line-height: 1.2em;
    margin: 0;
    padding: 15px 0;
    box-sizing: border-box;
  }

  h3 {
    font-size: 1.2em;
    font-weight: 500;
    color: ${(props) => props.theme.textColor};
    line-height: 1.2em;
    margin: 0;
    box-sizing: border-box;
  }

  a{
    text-decoration: none;
    color: ${(props) => props.theme.primary};
    font-weight: bold;
  }

  p, ol {
    margin: 0;
  }

  .blue-text {
    color: ${(props) => props.theme.primary};
  }
`;
