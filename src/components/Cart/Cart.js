import React from 'react';
import { connect } from 'react-redux';
import { removeFromCart, removeAllFromCart } from 'store/actions/actions';

import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Dialog from '@material-ui/core/Dialog';
import DeleteIcon from '@material-ui/icons/Delete';

import {
  DialogHeaderWrapper,
  MuiDialogTitle,
  MuiListItemText,
  ListPrice,
  DeleteContainer,
  TotalContainer,
} from './CartStyle';

const Cart = (props) => {
  const { onClose, open } = props;

  const handleClose = () => {
    onClose();
  };

  const handleRemoveProduct = (item) => (event) => {
    props.removeFromCart(item);
  };

  const handleRemoveAllProducts = () => {
    props.removeAllFromCart();
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='simple-dialog-title'
      open={open}
      maxWidth='sm'
      fullWidth={true}
    >
      <DialogHeaderWrapper>
        <MuiDialogTitle id='simple-dialog-title'>Cart</MuiDialogTitle>
        <Button
          variant='contained'
          startIcon={<DeleteIcon />}
          onClick={handleRemoveAllProducts}
          style={{ height: '40px', marginRight: '20px' }}
        >
          Remove all
        </Button>
      </DialogHeaderWrapper>
      <List>
        {props.cart.map((item) => (
          <ListItem key={item.id + item.name} button>
            <ListItemAvatar>
              <Avatar src={item.imageUrl} />
            </ListItemAvatar>
            <MuiListItemText id={item.id} primary={item.name} />
            <ListPrice>
              {item.price}
              {item.currency}
            </ListPrice>
            <DeleteContainer>
              <DeleteIcon
                onClick={handleRemoveProduct(item)}
                style={{ cursor: 'pointer' }}
              />
            </DeleteContainer>
          </ListItem>
        ))}
      </List>

      <TotalContainer>Total: {props.cartTotal.toFixed(2)}</TotalContainer>
    </Dialog>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    cartTotal: state.cartTotal,
  };
};

export default connect(mapStateToProps, { removeFromCart, removeAllFromCart })(
  Cart
);
