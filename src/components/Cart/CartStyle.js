import styled from 'styled-components';

import DialogTitle from '@material-ui/core/DialogTitle';
import ListItemText from '@material-ui/core/ListItemText';

export const DialogHeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const MuiDialogTitle = styled(DialogTitle)`
  h2 {
    font-size: 1.7em;
    color: #167cda;
    font-weight: 600;
  }
`;

export const MuiListItemText = styled(ListItemText)`
  max-width: 70%;
  span {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }
`;

export const ListPrice = styled.span`
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;
  font-weight: bold;
`;

export const DeleteContainer = styled.span`
  display: flex;
  padding: 5px;
`;

export const TotalContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 20px;

  font-size: 1.2em;
  font-weight: bold;

  border-top: 1px solid grey;
`;
