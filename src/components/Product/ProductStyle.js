import styled, { css } from 'styled-components';
import { theme } from 'GlobalStyles';

export const ProductContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 500px;
  box-sizing: border-box;
  overflow: hidden;

  background-color: white;
  transition: height 0.4s ease-out;
  box-shadow: 0px 2px 34px rgba(121, 121, 121, 0.5);

  transition: transform 0.2s;

  :hover {
    transform: scale(1.01);
  }
`;

export const ProductAvatarContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60%;

  ${(props) =>
    props.img &&
    css`
      background-image: url(${(props) => props.img});
      background-repeat: no-repeat;
      background-position: center;
      background-size: 100%;
    `}
`;

export const ProductTitle = styled.h2`
  font-size: 1.7em;
  color: black;
  font-weight: 600;
  padding: 0;
  margin: 0;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

export const ProductText = styled.span`
  display: flex;
  flex-grow: 1;
  font-size: 0.8em;
  color: ${theme.textColor};
`;

export const ProductPrice = styled.span`
  display: flex;
  justify-content: flex-end;
  font-size: 1.2em;
  color: black;
`;

export const ProductInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 60%;
  box-sizing: border-box;
  padding: 40px 20px 20px 20px;
`;

export const ProductActionsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 10px;
`;

export const ProductAction = styled.div`
  display: flex;
  flex-direction: column;
  width: 48%;
  color: ${theme.textColor};
`;
