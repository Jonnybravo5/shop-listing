import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  addToWishlist,
  removeFromWishlist,
  addToCart,
  removeFromCart,
} from 'store/actions/actions';

import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

import {
  ProductContainer,
  ProductAvatarContainer,
  ProductTitle,
  ProductText,
  ProductPrice,
  ProductInfoContainer,
  ProductActionsWrapper,
  ProductAction,
} from './ProductStyle';

const Product = (props) => {
  const { imageUrl, name, description, price, currency } = props.product;

  const [isInWishlist, setIsInWishlist] = useState(false);
  const [isInCart, setIsInCart] = useState(false);

  useEffect(() => {
    setIsInCart(props.cart.some((product) => product.id === props.product.id));
  }, [props.cart, props.product.id]);

  const handleUpdateWishlist = () => {
    setIsInWishlist(!isInWishlist);

    isInWishlist ? props.removeFromWishlist() : props.addToWishlist();
  };

  const handleUpdateCart = () => {
    setIsInCart(!isInCart);

    isInCart
      ? props.removeFromCart(props.product)
      : props.addToCart(props.product);
  };

  return (
    <ProductContainer>
      <ProductAvatarContainer img={imageUrl} />
      <ProductInfoContainer>
        <ProductTitle title={name}>{name}</ProductTitle>
        <ProductText>{description}</ProductText>
        <ProductPrice>
          Price: {price}
          {currency}
        </ProductPrice>
        <ProductActionsWrapper>
          <ProductAction>
            <Button
              variant='contained'
              color='secondary'
              startIcon={<FavoriteIcon />}
              onClick={handleUpdateWishlist}
              style={{ textTransform: 'none', fontSize: '0.8em' }}
            >
              {isInWishlist ? 'Remove from Wishlist' : 'Add to Wishlist'}
            </Button>
          </ProductAction>
          <ProductAction>
            <Button
              variant='contained'
              color='primary'
              startIcon={<ShoppingCartIcon />}
              onClick={handleUpdateCart}
              style={{ textTransform: 'none', fontSize: '0.8em' }}
            >
              {isInCart ? 'Remove from Cart' : 'Add to Cart'}
            </Button>
          </ProductAction>
        </ProductActionsWrapper>
      </ProductInfoContainer>
    </ProductContainer>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

export default connect(mapStateToProps, {
  addToWishlist,
  removeFromWishlist,
  addToCart,
  removeFromCart,
})(Product);
