import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
// import { compose } from 'redux';
// import { connect } from 'react-redux';
// import { NavLink, withRouter } from 'react-router-dom';

import Badge from '@material-ui/core/Badge';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

import {
  HeaderStyle,
  HeaderWrapper,
  Logo,
  LogoImage,
  ButtonsContainer,
} from './HeaderStyle';

import Cart from 'components/Cart/Cart';

import logo from 'assets/img/logo.svg';

const Header = (props) => {
  const [openCart, setOpenCart] = useState(false);

  const handleClickOpen = () => {
    setOpenCart(true);
  };

  const handleClose = (value) => {
    setOpenCart(false);
  };

  return (
    <Fragment>
      <HeaderStyle>
        <HeaderWrapper>
          <Logo>
            <LogoImage src={logo} alt='logo' />
          </Logo>

          <ButtonsContainer>
            <Badge badgeContent={props.wishlist} color='secondary' showZero>
              <FavoriteIcon />
            </Badge>
            <Badge
              badgeContent={props.cart.length}
              color='primary'
              onClick={handleClickOpen}
              showZero
              style={{ cursor: 'pointer' }}
            >
              <ShoppingCartIcon />
            </Badge>
          </ButtonsContainer>
        </HeaderWrapper>
      </HeaderStyle>
      <Cart open={openCart} onClose={handleClose}></Cart>
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    wishlist: state.wishlist,
    cart: state.cart,
  };
};

export default connect(mapStateToProps)(Header);
