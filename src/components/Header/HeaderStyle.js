import styled from 'styled-components';
import { globalVars } from 'GlobalStyles';

export const HeaderStyle = styled.header`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  background-color: ${(props) => props.theme.primary};
  height: ${globalVars.headerHeight};
  color: white;
  z-index: 2;
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),
    0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12);
`;

export const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  z-index: 20;
  justify-content: space-between;
  width: ${globalVars.headerWidthPercentage};
  min-height: ${globalVars.headerHeight};
`;

export const Logo = styled.div`
  display: flex;
  justify-content: left;
  width: 50%;
`;

export const LogoImage = styled.img`
  height: 100%;
  width: 100%;
  max-width: 150px;
  object-fit: contain;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  height: 100%;
  width: 50%;
  padding: 0 10px;
  box-sizing: border-box;

  svg {
    color: black;
    font-size: 30px;
  }

  > span {
    margin: 0 10px;
  }
`;
