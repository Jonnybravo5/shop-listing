import React, { Fragment, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchProducts } from 'store/actions/actions';

import {
  SectionContainer,
  SectionTopWrapper,
  SectionTitle,
  SectionText,
  SectionContent,
  ProductContainer,
} from './ProductListStyle';

import Product from 'components/Product/Product';

const ProductList = () => {
  const dispatch = useDispatch();

  //STORE
  const products = useSelector((state) => state.products);

  //STATE
  const [isShowing, setIsShowing] = useState(true);

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  useEffect(() => {
    if (!isShowing) {
      alert('ecenas');
    }
  }, [isShowing]);

  const handleClick = (event) => {
    setIsShowing(!isShowing);
  };

  const listOfProducts = () => {
    let productList = products.map((product, idx) => {
      return <Product key={product.id + idx} product={product}></Product>;
    });

    return (
      <Fragment>
        <SectionTopWrapper>
          <SectionTitle>List of Products</SectionTitle>
          <SectionText>The list of available products</SectionText>
        </SectionTopWrapper>
        <SectionContent>
          <button onClick={handleClick}>TEST</button>
          {isShowing && <ProductContainer>{productList}</ProductContainer>}
        </SectionContent>
      </Fragment>
    );
  };

  return <SectionContainer>{listOfProducts()}</SectionContainer>;
};

export default ProductList;
