import styled from 'styled-components';
import { theme, globalVars } from 'GlobalStyles';

export const SectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  padding-bottom: 40px;
  box-sizing: border-box;
  overflow: auto;

  height: calc(100vh - ${globalVars.headerHeight});
  margin-top: ${globalVars.headerHeight};
`;

export const SectionTopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

export const SectionTitle = styled.h1`
  color: #167cda;
  padding: 20px 20px 0 20px;
  box-sizing: border-box;
`;

export const SectionText = styled.span`
  color: ${theme.textColorDark};
  padding: 0 20px;
  box-sizing: border-box;
  font-size: 1.2em;

  strong {
    font-weight: 600;
  }
`;

export const SectionContent = styled.div`
  display: ${(props) => (props.hidden ? 'hidden' : 'flex')};
  flex-direction: column;
  //height: 100vh;
  //overflow: auto;
  padding: 0px 20px 40px 40px;
`;

export const SectionFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 15px 25px;
  box-sizing: border-box;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
`;

export const ProductContainer = styled.div`
  display: grid;
  flex-grow: 1;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
`;
