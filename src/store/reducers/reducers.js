import { combineReducers } from 'redux';

const fetchProductsReducer = (state = [], action) => {
  if (action.type === 'FETCH_PRODUCTS') return action.payload;

  return state;
};

const updateCartReducer = (cartProducts = [], action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      return [...cartProducts, action.payload];
    case 'REMOVE_FROM_CART':
      return cartProducts.filter((element) => element.id !== action.payload.id);
    case 'REMOVE_ALL_FROM_CART':
      return [];
    default:
      return cartProducts;
  }
};

const updateWishlistReducer = (wishlistCounter = 0, action) => {
  switch (action.type) {
    case 'ADD_TO_WISHLIST':
      return wishlistCounter + 1;
    case 'REMOVE_FROM_WISHLIST':
      return wishlistCounter - 1 < 0 ? 0 : wishlistCounter - 1;
    default:
      return wishlistCounter;
  }
};

const updateCartTotalReducer = (cartTotal = 0, action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      return cartTotal + parseFloat(action.payload.price);
    case 'REMOVE_FROM_CART':
      return cartTotal - parseFloat(action.payload.price) < 0
        ? 0
        : cartTotal - parseFloat(action.payload.price);
    case 'REMOVE_ALL_FROM_CART':
      return 0;
    default:
      return cartTotal;
  }
};

export default combineReducers({
  products: fetchProductsReducer,
  cart: updateCartReducer,
  wishlist: updateWishlistReducer,
  cartTotal: updateCartTotalReducer,
});
