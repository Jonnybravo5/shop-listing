import axios from 'axios';

export const fetchProducts = () => async (dispatch) => {
  const response = await axios.get('/products.json');
  dispatch({
    type: 'FETCH_PRODUCTS',
    payload: response.data,
  });
};

export const addToCart = (item) => {
  return {
    type: 'ADD_TO_CART',
    payload: item,
  };
};

export const addToWishlist = (item) => {
  return {
    type: 'ADD_TO_WISHLIST',
    payload: item,
  };
};

export const removeFromCart = (item) => {
  return {
    type: 'REMOVE_FROM_CART',
    payload: item,
  };
};

export const removeAllFromCart = () => {
  return {
    type: 'REMOVE_ALL_FROM_CART',
  };
};

export const removeFromWishlist = (item) => {
  return {
    type: 'REMOVE_FROM_WISHLIST',
    payload: item,
  };
};
